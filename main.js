import * as boucle from "./exercices/boucles.js";
boucle.ex1();
boucle.ex2();
boucle.ex3();
boucle.ex4();
boucle.ex5();
boucle.ex6();
boucle.ex7();
boucle.ex8();

import * as fonction from "./exercices/fonctions.js";
console.log(fonction.ex1());
fonction.ex2('test, un, deux...');
fonction.ex3('ceci est une concaténation', 'de deux chaines de caractères');
fonction.ex4(4, 5);
fonction.ex4(4, 4);
fonction.ex4(4, 2);
fonction.ex5(234, 'une chaine de caractères');
fonction.ex6('Nom', 'Prénom', 40);
fonction.ex7(16, 'femme');
fonction.ex7(56, 'homme');
fonction.ex7('chaise', 'homme');
fonction.ex8(5, 4);

import * as tableau from "./exercices/tableaux.js"
console.log(tableau.moisDeLAnnee[3]);
console.log(tableau.moisDeLAnnee[5]);
tableau.moisDeLAnnee[7] = 'Août';
console.log(tableau.moisDeLAnnee);
console.log(tableau.hautsDeFrance);
console.log(tableau.hautsDeFrance['59']);
tableau.hautsDeFrance['51'] = "Reims";
console.log(tableau.hautsDeFrance);
tableau.ex8(tableau.moisDeLAnnee);
tableau.ex10(tableau.hautsDeFrance);