// Exercice 1 Faire une fonction qui retourne true.
export const ex1 = () => {
    return true;
}

// Exercice 2 Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.
export const ex2 = chaineDeCaracteres => {
    console.log(chaineDeCaracteres);
}

// Exercice 3 Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.

export const ex3 = (chaineDeCaracteres1, chaineDeCaracteres2) => {
        console.log(chaineDeCaracteres1 + ' ' + chaineDeCaracteres2);
    }
    // Exercice 4 Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :

//     Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
//     Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
//     Les deux nombres sont identiques si les deux nombres sont égaux
export const ex4 = (nombre1, nombre2) => {
        if (nombre1 > nombre2) {
            console.log(nombre1 + ' est plus grand que ' + nombre2);
        } else if (nombre1 < nombre2) {
            console.log(nombre1 + ' est plus petit que ' + nombre2)
        } else {
            console.log(nombre1 + ' et ' + nombre2 + ' sont identiques');
        }
    }
    // Exercice 5 Faire une fonction qui prend en paramètre un nombre et 
    // une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.
export const ex5 = (nombre, chaineDeCaracteres) => {
    console.log(nombre + ' ' + chaineDeCaracteres);
}

// Exercice 6 Faire une fonction qui prend trois paramètres : nom, prenom et age. 
// Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".
export const ex6 = (nom, prenom, age) => {
    console.log('Bonjour ' + nom + ' ' + prenom + ', tu as ' + age + ' ans');
}

// Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

//     Vous êtes un homme et vous êtes majeur
//     Vous êtes un homme et vous êtes mineur
//     Vous êtes une femme et vous êtes majeur
//     Vous êtes une femme et vous êtes mineur
export const ex7 = (age, genre) => {
    if (genre == "femme") {
        if (age < 0) {
            console.log('C\'est bizarre');
        } else if (age < 18) {
            console.log('Vous êtes une femme et vous êtes mineure');
        } else {
            console.log('Vous êtes une femme et vous êtes majeure');
        }
    } else if (genre == "homme") {
        if (age < 0) {
            console.log('C\'est bizarre');
        } else if (age < 18) {
            console.log('Vous êtes un homme et vous êtes mineur');
        } else {
            console.log('Vous êtes un homme et vous êtes majeur')
        }
    } else {
        console.log('C\'est bizarre');
    }
}

// Exercice 8 Faire une fonction qui prend en paramètre trois nombres 
// et qui renvoit la somme de ces nombres. Tous les paramètres doivent avoir une valeur par défaut.

export const ex8 = (nombre1 = 2, nombre2 = 5, nombre3 = 10) => {

    console.log(nombre1 + nombre2 + nombre3);
}