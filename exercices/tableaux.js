// Exercice 1 Créer un tableau mois et l'initialiser avec le nom des douze mois de l'année.
export let moisDeLAnnee = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];

// Exercice 2 Avec le tableau de l'exercice 1, afficher la valeur de la troisième ligne de ce tableau.=>main

// Exercice 3 Avec le tableau de l'exercice , afficher la valeur de l'index 5. =>main
// Exercice 4 Avec le tableau de l'exercice 1, modifier le mois de aout pour lui ajouter l'accent manquant.=>main

// Exercice 5 Créer un tableau associatif avec comme index le numéro des départements des Hauts de France et en valeur leur nom.

export let hautsDeFrance = { "02": "Aisne", "59": "Nord", "60": "Oise", "62": "Pas-de-Calais", "80": "Somme" };

// Exercice 6 Avec le tableau de l'exercice 5, afficher la valeur de l'index 59.=>main
// Exercice 7 Avec le tableau de l'exercice 5, ajouter la ligne correspondant au département de la ville de Reims.=>main
// Exercice 8 Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau.
export let ex8 = tab => { tab.forEach(element => console.log(element)) };

// Exercice 9 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau.=>main
// Exercice 10 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau 
// ainsi que les clés associés. Cela pourra être, par exemple, de la forme : 
// "Le département" + nom_departement + "a le numéro" + num_departement
export const ex10 = tab => {
    for (let numero in tab) {
        console.log('le département ' + tab[numero] + ' a le numéro ' + numero);
    }
}