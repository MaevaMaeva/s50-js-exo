// Exercice 1 Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :

//     l'afficher
//     incrémenter de 1


export let ex1 = () => {
    let x = 0;
    while (x < 10) {
        console.log(x);
        x++;
    }
}



// Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec 
// un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :

//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     incrémenter la première variable

export let ex2 = () => {
    let deuxieme = Math.floor(Math.random(1, 100) * 100);
    for (let premiere = 0; premiere <= 20; premiere++) {
        console.log(premiere * deuxieme);
    }

}

// Exercice 3 Créer deux variables. 
// Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. 
// Tant que la première variable n'est pas inférieur ou égale à 10 :

//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     décrémenter la première variable
export const ex3 = () => {
    let second = Math.floor(Math.random(1, 100) * 100);
    for (let first = 100; first >= 10; first--) {
        console.log(first * second);
    }
}

// Exercice 4 Créer une variable et l'initialiser à 1. 
//Tant que cette variable n'atteint pas 10 :

//     l'afficher
//     l'incrementer de la moitié de sa valeur

export const ex4 = () => {
        for (let variable1 = 1; variable1 < 10; variable1 += variable1 / 2) {
            console.log(variable1);
        }
    }
    // Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...
export const ex5 = () => {
    for (let unPas = 1; unPas <= 15; unPas++) {
        console.log('On y arrive presque...');
    }
}

// Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...
export const ex6 = () => {
        for (let recule = 20; recule > 0; recule--) {
            console.log('C\'est presque bon...');
        }
    }
    // Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...

export const ex7 = () => {
    for (let grandPas = 1; grandPas < 100; grandPas += 15) {
        console.log('On tient le bon bout...');
    }
}

// Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !
export const ex8 = () => {
    for (let grandReculons = 200; grandReculons > 0; grandReculons -= 20) {
        console.log('Enfin ! ! !');
    }
}